# Practical guide to computer simulations

This repository contains examples and solution programs from the book
A.K. Hartmann "Practical guide to computer simulations"
(World Scientific, 2008).
For each chapter of the book, a subdirectory exists, as follows

Directory  |    Chapter name
-----------|------------------------------------------------
`algorithms` |    Algorithms and data structures
`debugging`  |    Debugging and Testing
`literature` |    Information Retrieval, Publishing and Presentations
`randomness` |    Randomness and Statistics
`se`         |    Software Engineering
`c-programming` | Programming in C
`libraries`  |    Libraries
`oop`        |    Object-oriented Software Development

A PDF file is kindly provided by the author. The file is closely related to the book's content. This document may be distributed freely in electronic and nonelectronic
form, provided that no changes are performed to it.
