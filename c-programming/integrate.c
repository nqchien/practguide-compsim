/*******************************************************************/
/*** Integrates a function by simply approximating it by a sum   ***/
/*** (solution to exercise)                                      ***/
/***                                                             ***/
/*** Sample programs from the book:                              ***/
/*** A.K. Hartmann                                               ***/
/*** A practical guide to computer simulation                    ***/
/*** World Scientific, Singapore 2008                            ***/
/***                                                             ***/
/*** Chapter: Programming in C                                   ***/
/*** Section: Exercises                                          ***/
/*******************************************************************/

#include <stdio.h>
#include <math.h>

/******************** integrate() *********************/
/** Integrates a 1-dim function numerically using    **/
/** the rectangular rule                             **/
/** PARAMETERS: (*)= return-paramter                 **/
/**        from: startpoint of interval              **/
/**          to: endpoint of interval                **/
/**   num_steps: number of integration steps         **/
/**           f: p. to function to be integrated     **/
/** RETURNS:                                         **/
/**     value of integral                            **/
/******************************************************/
double integrate(double from, double to, int num_steps, 
		 double (* f)(double))
{
  int step;                                    /* loop counter */
  double delta;              /* width of one integrateion step */
  double sum;                              /* integrated value */
  double x;                                /* current argument */
  
  delta = (to-from)/num_steps;
  sum = 0.0;
  x = from;
  for(step=0; step<num_steps; step++)   /* perform integration */
  {
      sum += delta*(f(x)+f(x+delta))/2;
      x += delta;
  }
  return(sum);
}


int main()
{
  int num_steps = 1000;          /* number of integration steps */

  printf("Int(0,PI/2) sin(x) = %f\n", 
	 integrate(0, M_PI/2, num_steps, sin));
  printf("Int(0,PI) sin(x) = %f\n", 
	 integrate(0, M_PI, num_steps, sin));
  printf("Int(0,1.5*PI) sin(x) = %f\n", 
	 integrate(0, 1.5*M_PI, num_steps, sin));
  printf("Int(0,2*PI) sin(x) = %f\n", 
	 integrate(0, 2*M_PI, num_steps, sin));

  return(0);
}
