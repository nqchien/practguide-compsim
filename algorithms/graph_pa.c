/****************************************************************/
/*** Main program for undirected graphs                       ***/
/*** A.K. Hartmann January 2008                               ***/
/*** Forgeschrittene Computersimulationen                     ***/
/*** University of Oldenburg, Germany 2008                    ***/
/****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "list.h"
#include "graphs.h"

int main(int argc, char **argv)
{
  int num_nodes;                             /* number of nodes in graph */
  int num_edges;                             /* number of edges in graph */
  gs_graph_t *g;
  int t, n;                                             /* loop counters */
  int num_real;                                /* number of realizations */
  int argz = 1;                   /* for treating command line arguments */
  int *histo;                                   /* histogram for degrees */
  int num_entries;                                       /* in histogram */
  int d_max;                                           /* maximum degree */
  char name[1000], filename[1000], command[1000];   /* auxiliary strings */
  FILE *file;                                          /* to write graph */


  if(argc < 3)
  {
    printf("Usage %s <number nodes> <(initial) number edges>\n", argv[0]);
    exit(1);
  }

  num_real = 1;
  num_nodes = atoi(argv[argz++]);                /* read number of nodes */
  num_edges =  atoi(argv[argz++]);    /* number of initially added edges */

  d_max = num_nodes;
  histo = (int *) malloc(d_max*sizeof(int));           /* init histogram */
  for(t=0;t < d_max; t++)
      histo[t] = 0;

  g = gs_create_graph(num_nodes);

  num_entries = 0;
  for(t=0; t<num_real; t++)
  {
    gs_clear_graph(g);                          /* generate random graph */

    /* uncomment the following line for preferential attachment */
    gs_preferential_attachment(g, num_edges);
    /* uncomment the following line for random graphs */
    /* gs_random_graph(g, num_edges); */

    for(n=0; n<g->num_nodes; n++)       /* insert degrees into histogram */
    {
      if(gs_degree(g, n)< d_max)
      {
	histo[gs_degree(g, n)]++;
	num_entries++;
      }
    }
    /*gs_write_graph(g, stdout);*/
  }
  strcpy(name, "testgraph");
  sprintf(filename, "%s.dot", name);
  file = fopen(filename, "w");
  gs_dot_graph(g, file, 0);
  fclose(file);
  sprintf(command, "dot -Tps %s.dot > %s.ps", name, name);
  system(command);
  /*gs_dot_graph(g, stdout, 0);*/
  exit(1);

  for(t=0; t<d_max; t++)                              /* print histogram */
    if(histo[t] > 0)
      printf("%d %f\n", t, (double) histo[t]/num_entries);



  gs_delete_graph(g);
  free(histo);

  return(0);

}
